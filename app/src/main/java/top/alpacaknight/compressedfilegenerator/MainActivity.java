package top.alpacaknight.compressedfilegenerator;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    //相关的测试路径
    private static final String zipPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/infosec/file.zip";//保存的压缩文件路径
    private static final String unzipPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/infosec/temp/test/";//解压的根目录
    private static final String srcFiles = Environment.getExternalStorageDirectory().getAbsolutePath() + "/infosec/temp/";//将要压缩的路径

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        //使用示例
        //zipFile();
        zipFileList();
        upZipFile();
    }

    /**
     * 压缩一个文件或者目录
     * */
    public static void zipFile(){
        File file = new File(zipPath);
        if(file.exists())
            file.delete();
        ZipUtils.zipFilesByPath(srcFiles, zipPath);
    }

    /**
     * 压缩一个列表所包含的所有文件或目录
     * */
    public static void zipFileList(){
        File file = new File(zipPath);
        if(file.exists()){
            file.delete();
        }
        //生成要压缩的目录路径列表
        ArrayList<String> pathList = new ArrayList<String>();
        pathList.add(Environment.getExternalStorageDirectory().getAbsolutePath() + "/infosec/image/");
        pathList.add(Environment.getExternalStorageDirectory().getAbsolutePath() + "/infosec/pdf/");
        pathList.add(Environment.getExternalStorageDirectory().getAbsolutePath() + "/infosec/html/");
        pathList.add(Environment.getExternalStorageDirectory().getAbsolutePath() + "/infosec/temp/");
        ZipUtils.zipFileByPathList(pathList, zipPath);
    }

    /**
     * 解压缩一个文件
     * */
    public static void upZipFile(){
        try {
            ZipUtils.unzipFileToPath(zipPath, unzipPath + File.separator);//自己修改优化的解压方法
            //ZipUtils.unzipFilesWithTier(ZipUtils.readFileByte(zipPath), unzipPath + File.separator);(网络上摘抄的解压代码)
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
